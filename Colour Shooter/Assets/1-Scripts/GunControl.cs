﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunControl : MonoBehaviour {

	public Vector2 mousePos;
	public Vector2 myPos;
	public Vector2 direction;
	public float angle;

	public GameObject shooter;
	public LineRenderer laser;
	public SpriteRenderer colorPreview;
	public Color[] laserColor = {Color.red, Color.green, Color.cyan};

	public CircleCollider2D laserCollider;
	
	public int colorIndex = 0;
	public int dmg;

	public RaycastHit2D hitInfo;

	public bool greenLaser = false;
	public bool blueLaser = false;
	
	private bool shoot = false;
	private float shootTime;
	// Use this for initialization
	void Start () {

		laser.enabled = shoot;
		laserCollider.enabled = shoot;		
	}
	
	// Update is called once per frame
	void Update () 
	{
		myPos = (transform.position);
		mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		direction = (mousePos - myPos);

		transform.right = direction;


		if (Input.GetButtonDown("Fire1") && shoot == false)
		{
			shoot = true;
			shootTime = Time.time;
			ShootLaser();
		}
		ColorChange();
		NarrowLaser();
		
	}

	public void ShootLaser()
	{
		hitInfo = Physics2D.Raycast(shooter.transform.position, direction);
		laser.SetPosition(0,(shooter.transform.position));

		if(hitInfo)
		{
			laser.SetPosition(1, hitInfo.point);
			laserCollider.enabled = true;
			laserCollider.gameObject.transform.position = hitInfo.point;	

		} else
		{
			laser.SetPosition(1, direction * 100);
		}
	}

	public void NarrowLaser()
	{
		if (shoot)
		{
			laser.enabled = true;
			laser.startWidth = 0.2f - (Time.time - shootTime);
			laser.endWidth = laser.startWidth;

			if (laser.startWidth <= 0.01f)
			{
				laser.enabled = false;
				laserCollider.gameObject.transform.position = shooter.transform.position;
				laserCollider.enabled = false;
				shoot = false;
			}
		}
	}

	public void ColorChange()
	{
		if (Input.GetKeyDown(KeyCode.Q)) { colorIndex--; }
		if (Input.GetKeyDown(KeyCode.E)) { colorIndex++; }

		if(!greenLaser && !blueLaser) {colorIndex = 0; }

		if (colorIndex > 1 && greenLaser && !blueLaser) { colorIndex = 0; }
		if (colorIndex < 0 && greenLaser && !blueLaser) { colorIndex = 1; }

		if (colorIndex > 2 && blueLaser) { colorIndex = 0; }
		if (colorIndex < 0 && blueLaser) { colorIndex = 2; }
		
		laser.startColor = laserColor[colorIndex];
		colorPreview.color = laser.startColor;
		laser.endColor = Color.white;
		
	}

	public Color LaserColor()
	{
		return laser.startColor;
	}
	
	public void SetDamage(int _value)
	{
		dmg = _value;
	}
	public int GetDMG()
	{
		return dmg;
	}

	public void SetGreenLaser(bool _value) { greenLaser = _value; }
	public void SetBlueLaser(bool _value){ blueLaser = _value; }
}
