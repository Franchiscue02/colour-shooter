﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

	public float speed;
	public float diffToChange;
	public Transform initialPos;
	public Transform finalPos;

	public bool startPos;
	// Use this for initialization
	void Start () 
	{
		transform.position = initialPos.position;		
	}
	
	// Update is called once per frame
	void Update () {

		Movement();
		
	}

	public void Movement()
	{
		Vector2 iniPosV = Camera.main.ScreenToWorldPoint(initialPos.position);
		Vector2 finPosV = Camera.main.ScreenToWorldPoint(finalPos.position);
		Vector2 platfPosV = Camera.main.ScreenToWorldPoint(transform.position);

		Vector2 iniDiff = iniPosV - platfPosV;
		Vector2 iniDiffNorm = iniDiff.normalized;
		Vector2 finDiff = finPosV - platfPosV;
		Vector2 finDiffNorm = finDiff.normalized;

		if(Mathf.Abs(finDiff.magnitude) <= diffToChange)
		{
			startPos = false;
		}
		if (Mathf.Abs(iniDiff.magnitude) <= diffToChange)
		{
			startPos = true;
		}

		if(startPos)
		{
			transform.Translate((finPosV - platfPosV) * speed * Time.deltaTime);
		}
		if(startPos == false)
		{
			transform.Translate((iniPosV - platfPosV) * speed * Time.deltaTime);
		}
	}
}
