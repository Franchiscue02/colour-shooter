﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeLikeMonster : SetColor {

	public Transform initialPos, finalPos;
	public float speed;
	[Header("Direction to go if no final position")]
	public Vector2 direction;
	[Header ("Distance to position to move other way")]
	public float diffToChange;

	private bool startPos;
	// Use this for initialization
	protected override void Start () 
	{
		base.Start();
		transform.position = initialPos.position;	
	}
	
	// Update is called once per frame
	protected override void Update () 
	{
		base.Update();
		Movement();
	}

	public void Movement()
	{
		if (finalPos == null)
		{
			transform.Translate(direction * speed * Time.deltaTime);
		} else
		{
			Vector2 iniPosV = Camera.main.ScreenToWorldPoint(initialPos.position);
			Vector2 finPosV = Camera.main.ScreenToWorldPoint(finalPos.position);
			Vector2 platfPosV = Camera.main.ScreenToWorldPoint(transform.position);

			Vector2 iniDiff = iniPosV - platfPosV;
			Vector2 iniDiffNorm = iniDiff.normalized;
			Vector2 finDiff = finPosV - platfPosV;
			Vector2 finDiffNorm = finDiff.normalized;

			if(Mathf.Abs(finDiff.magnitude) <= diffToChange)
			{
				startPos = false;
			}
			if (Mathf.Abs(iniDiff.magnitude) <= diffToChange)
			{
				startPos = true;
			}

			if(startPos)
			{
				transform.Translate((finPosV - platfPosV) * speed * Time.deltaTime);
				if (mySprite.flipX)
				{
					mySprite.flipX = false;
				}
			}
			if(startPos == false)
			{
				transform.Translate((iniPosV - platfPosV) * speed * Time.deltaTime);

				if (!mySprite.flipX)
				{
					mySprite.flipX = true;
				}
			}
		}
	}
}
