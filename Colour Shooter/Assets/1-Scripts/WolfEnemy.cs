﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfEnemy : SetColor {

	public int hp;
	public bool dead = false;

	private float deathTime;
	private GunControl gunControl;
	// Use this for initialization
	protected override void Start () 
	{
		base.Start();	
	}
	
	// Update is called once per frame
	protected override void Update () 
	{
		base.Update();
		Disable();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Laser"))
		{
			gunControl = other.GetComponentInParent<GunControl>();

			if (mySprite.color == gunControl.LaserColor())
			{
				hp -= gunControl.GetDMG();
				Death();
			}
		}
	}

	public void Death()
	{
		if (hp < 1)
		{
			deathTime = Time.time;
			dead = true;
			transform.GetComponent<PolygonCollider2D>().enabled = false;
		}
	}

	public void Disable()
	{
		if(dead)
		{
			if (Time.time >= deathTime + 2.0f)
			{
				gameObject.SetActive(false);
			}
		}
	}
}
