﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetColor : MonoBehaviour {

	[Header("0 = Red, 1 = Green, 2 = Blue")]
	public int selectColor;

	protected SpriteRenderer mySprite;
	private Color[] myColor= {Color.red, Color.green, Color.cyan};
	// Use this for initialization
	protected virtual void Start () {

		mySprite = GetComponent<SpriteRenderer>();	
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		
		mySprite.color = myColor[selectColor];
	}
}
