﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour {

	public GameObject player;

	private GameObject searchPlayer;
	// Use this for initialization
	void Awake () {
		
		searchPlayer = GameObject.FindGameObjectWithTag("Player");

		if (searchPlayer == null)
		{
			Instantiate(player, transform.position, Quaternion.identity);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
