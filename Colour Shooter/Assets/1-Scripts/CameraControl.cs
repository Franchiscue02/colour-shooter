﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	public Transform target;
	public Transform cam;
	[Header("0 for Player, 1 for Camera")]
	public int selectTarget;

	public float smoothSpeed;
	public Vector3 offset;
	// Use this for initialization
	void Start () 
	{
		target = GameObject.FindGameObjectWithTag("Player").transform;
		target = target.Find("CameraFollow").transform;
		cam = Camera.main.transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (selectTarget == 0)
		{
			Vector3 desiredPos = target.position + offset;
			Vector3 smoothedPos = Vector3.Lerp(transform.position, desiredPos, smoothSpeed * Time.fixedDeltaTime);

			transform.position = smoothedPos;
		}
		if (selectTarget == 1)
		{
			Vector3 desiredPos = cam.position + offset;
			Vector3 smoothedPos = Vector3.Lerp(transform.position, desiredPos, smoothSpeed * Time.fixedDeltaTime);

			transform.position = smoothedPos;
		}
		
	}
}
