﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour {

	public float speed;
	public GameObject movingOBJ;
	public Transform initialPos;
	public Transform finalPos;

	public bool activated = false;
	
	[Header("0 = Red, 1 = Green, 2 = Blue")]
	public int colorIndex;
	

	private Color[] colors = {Color.red, Color.green, Color.blue};
	private SpriteRenderer mySprite, objSprite;

	// Use this for initialization
	void Start () {
		
		movingOBJ.transform.position = initialPos.position;

		mySprite = GetComponent<SpriteRenderer>();
		objSprite = movingOBJ.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		MoveObject();
		SetColors();
	}

	public void MoveObject()
	{
		Vector2 iniPosV = Camera.main.ScreenToWorldPoint(initialPos.position);
		Vector2 finPosV = Camera.main.ScreenToWorldPoint(finalPos.position);
		Vector2 platfPosV = Camera.main.ScreenToWorldPoint(movingOBJ.transform.position);

		Vector2 iniDiff = (iniPosV - platfPosV).normalized;
		Vector2 finDiff = (finPosV - platfPosV).normalized;

		if(activated == false)
		{
			movingOBJ.transform.Translate((iniPosV - platfPosV) * speed * Time.deltaTime);
		} else
		{
			movingOBJ.transform.Translate((finPosV - platfPosV) * speed * Time.deltaTime);
		}

	}

	public void SetColors()
	{
		mySprite.color = colors[colorIndex];
		objSprite.color = mySprite.color;
	}

	private void OnTriggerEnter2D(Collider2D other) 
	{	
		if (other.CompareTag("Laser"))
		{
			GunControl gunControl = other.gameObject.GetComponentInParent<GunControl>();

			if (gunControl.LaserColor() == objSprite.color)
			{
				activated = !activated;
			}
		}
	}
}
