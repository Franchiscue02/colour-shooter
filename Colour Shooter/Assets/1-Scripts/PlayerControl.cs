﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {


	public float speed;
	public float horInputValue;
	public string horInputName;
	public Vector3 horVector;

	public float jumpForce;
	public float jumpForceMultiplier;
	public int jumpCount;
	public int maxJumps;
	public string jumpInputName;
	public Vector3 vertVector;

	public int lifePoints;

	public GameObject laserGun;
	[Header("GO the Camera lerps")]
	public GameObject cameraFollow;
	public float distanceToPlayer;

	private Rigidbody2D myRB;
	private bool haveGun = false;
	// Use this for initialization
	void Start () 
	{
		myRB = GetComponent<Rigidbody2D>();
		SetGun(true);
	}
	
	// Update is called once per frame
	void Update () {

		Movement();
		PosCameraFollow();	
	}

	public void Movement()
	{
		horInputValue = Input.GetAxis(horInputName);

		transform.Translate(horVector * horInputValue * speed * Time.deltaTime);

		Jump();

	}

	public void Jump()
	{
		if(Input.GetButtonDown(jumpInputName) && jumpCount < maxJumps)
		{
			jumpCount++;
			myRB.AddForce(vertVector * jumpForce * jumpForceMultiplier * Time.deltaTime, ForceMode2D.Impulse);
		}

		if (myRB.velocity.y == 0)
		{
			jumpCount = 0;
		}
	}

	public void SetGun(bool _value)
	{
		haveGun = _value;

		if (haveGun)
		{
			laserGun.SetActive(true);
		} else 
		{
			laserGun.SetActive(false);
		}
	}
	
	public void PosCameraFollow()
	{
		cameraFollow.transform.position = transform.position + (transform.right * horInputValue) * distanceToPlayer + transform.up * (Mathf.Clamp(myRB.velocity.y, -1.0f, 1.0f)) * distanceToPlayer/2;
	}

	private void OnCollisionStay2D(Collision2D other)
	{
		if (other.collider.CompareTag("Platform"))
		{
			transform.SetParent(other.gameObject.transform);
		}
	}

	private void OnCollisionExit2D(Collision2D other) 
	{
		if(other.collider.CompareTag("Platform"))
		{
			transform.SetParent(null);
		}
	}
}
